package com.sell.demo.dao;

import com.sell.demo.enums.OrderStatusEnums;
import com.sell.demo.enums.PayStatusEnums;
import com.sell.demo.polo.OrderMaster;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderMasterDaoTest {
    @Autowired
    private OrderMasterDao orderMasterDao;
    @Test
    public void findByBuyerOpenid() {
        PageRequest pageRequest  = new PageRequest(0,1);
        Page<OrderMaster> orderMasterPage = orderMasterDao.findByBuyerOpenid("12345678",pageRequest);
        System.out.println(orderMasterPage.getTotalElements());
    }

    @Test
    public void save(){
        OrderMaster orderMaster = new OrderMaster();
        orderMaster.setOrderId("12345");
        orderMaster.setBuyerName("骆迪兴");
        orderMaster.setBuyerAddress("韩山师范学院东丽A");
        orderMaster.setBuyerPhone("15363386051");
        orderMaster.setBuyerOpenid("123456789");
        orderMaster.setOrderAmount(new BigDecimal(30));
        orderMaster.setOrderStatus(OrderStatusEnums.NEW.getCode());
        orderMaster.setPayStatus(PayStatusEnums.WAIT.getCode());
        OrderMaster result = orderMasterDao.save(orderMaster);
        Assert.assertEquals("骆迪兴",result.getBuyerName());
    }


}