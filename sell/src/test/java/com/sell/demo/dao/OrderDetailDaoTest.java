package com.sell.demo.dao;

import com.sell.demo.polo.OrderDetail;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderDetailDaoTest {
    @Autowired
    private OrderDetailDao orderDetailDao;
    @Test
    public void findByOrderId() {
        List<OrderDetail> orderDetailList = orderDetailDao.findByOrderId("123");
        Assert.assertEquals(1,orderDetailList.size());
    }

    @Test
    public void save(){
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setOrderId("123");
        orderDetail.setDetailId("123");
        orderDetail.setProductId("123");
        orderDetail.setProductQuantity(3);
        orderDetail.setProductName("生菜");
        orderDetail.setProductPrice(new BigDecimal(1.0));
        orderDetail.setProductIcon("http://xxxx");
        OrderDetail result = orderDetailDao.save(orderDetail);
        Assert.assertEquals("生菜",result.getProductName());
    }
}