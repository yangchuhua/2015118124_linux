package com.sell.demo.dao;

import com.sell.demo.polo.ProductCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductCategoryDaoTest {
    @Autowired
   private ProductCategoryDao productCategoryDao;

    @Test
    public void findOne(){
        ProductCategory productCategory = productCategoryDao.findById(1).get();
        System.out.print(productCategory.toString());
    }

//    @Test
//    public void saveOne(){
//        ProductCategory productCategory = new ProductCategory();
//        productCategory.setCategory_id(2);
//        productCategory.setCategory_name("薯片");
//        productCategory.setCategory_type(4);
//        productCategoryDao.save(productCategory);
//    }

    @Test
    public void findByCategoryTypeInTest(){
        List<Integer> list = Arrays.asList(1);
        List<ProductCategory> result = productCategoryDao.findByCategoryTypeIn(list);
        Assert.assertNotEquals(0,result.size());
        System.out.println(result.toString());
    }
}