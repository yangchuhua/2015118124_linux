package com.sell.demo.dao;

import com.sell.demo.polo.SellerInfo;
import com.sell.demo.util.KeyUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class SellInfoDaoTest {
    @Autowired
    private SellInfoDao sellInfoDao;

    @Test
    public void save(){
        SellerInfo sellerInfo = new SellerInfo();
        sellerInfo.setId(KeyUtil.genUniqueKey());
        sellerInfo.setUsername("杨楚哗");
        sellerInfo.setPassword("杨楚哗");
        sellerInfo.setOpenid("yangchuhua");
        SellerInfo result = sellInfoDao.save(sellerInfo);
        Assert.assertEquals("杨楚哗",result.getUsername());
    }
    @Test
    public void findByUsernameAndPassword() {
        SellerInfo sellerInfo = sellInfoDao.findByUsernameAndPassword("杨楚哗","杨楚哗");
        Assert.assertEquals("yangchuhua",sellerInfo.getOpenid());
    }
}