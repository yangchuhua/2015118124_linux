package com.sell.demo.dao;

import com.sell.demo.enums.ProductStatusEnums;
import com.sell.demo.polo.ProductInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductInfoDaoTest {
    @Autowired
    private ProductInfoDao productInfoDao;

    @Test
    public void findOne(){
        ProductInfo productInfo = productInfoDao.findById("123").get();
        //System.out.print(productInfo.toString());
        Assert.assertEquals("黑米粥",productInfo.getProductName());

    }

    @Test
    public void save(){
        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductId("124");
        productInfo.setProductName("小米粥");
        productInfo.setProductPrice(new BigDecimal(3.2));
        productInfo.setProductStock(2);
        productInfo.setProductDescription("好吃的小米粥");
        productInfo.setProductIcon("http:xx");
        productInfo.setProductStatus(1);
        productInfoDao.save(productInfo);
       // Assert.assertEquals(123,productInfo1.getProductId());
    }
    @Test
    public void findByProductStatus() {
        List<ProductInfo> productInfoList = productInfoDao.findByProductStatus(ProductStatusEnums.DOWN.getCode());
        Assert.assertEquals(2,productInfoList.size());

    }
}