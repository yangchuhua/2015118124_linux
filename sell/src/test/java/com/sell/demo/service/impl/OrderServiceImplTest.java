package com.sell.demo.service.impl;

import com.sell.demo.dto.OrderDTO;
import com.sell.demo.enums.OrderStatusEnums;
import com.sell.demo.enums.PayStatusEnums;
import com.sell.demo.enums.ResultEnums;
import com.sell.demo.polo.OrderDetail;
import com.sell.demo.polo.OrderMaster;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceImplTest {

     @Autowired
     private OrderServiceImpl orderServiceImpl;
    @Test
    public void create() {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setBuyerAddress("韩山师范学院");
        orderDTO.setBuyerName("杨楚哗111");
        orderDTO.setBuyerOpenid("123445511111");
        orderDTO.setBuyerPhone("12345678911");

        List<OrderDetail> orderDetailList = new ArrayList<>();
        OrderDetail orderDetail = new OrderDetail();
        OrderDetail orderDetail1 = new OrderDetail();

        orderDetail.setProductId("125");
        orderDetail.setProductQuantity(3);

        orderDetail1.setProductId("124");
        orderDetail1.setProductQuantity(1);
        orderDetailList.add(orderDetail);
        orderDetailList.add(orderDetail1);
        orderDTO.setOrderDetailList(orderDetailList);
        OrderDTO result = orderServiceImpl.create(orderDTO);
    }

    @Test
    public void findOne() {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO = orderServiceImpl.findOne("1528004383251229489");
        Assert.assertEquals("1234455",orderDTO.getBuyerOpenid());

    }

    @Test
    public void findList() {
        PageRequest pageRequest = new PageRequest(0,2);
        Page<OrderDTO> orderDTOPage = orderServiceImpl.findList("123456789",pageRequest);
        System.out.println(orderDTOPage);
        Assert.assertEquals(1,orderDTOPage.getTotalElements());
    }

    @Test
    public void pay() {
        OrderDTO orderDTO = orderServiceImpl.findOne("1528004887196943161");
        OrderDTO result = orderServiceImpl.pay(orderDTO);
        Assert.assertEquals(PayStatusEnums.SUCCESS.getCode(),result.getPayStatus());
    }

    @Test

    public void cancle() {
        OrderDTO orderDTO = orderServiceImpl.findOne("1528090843747868670");
        OrderDTO result = orderServiceImpl.cancle(orderDTO);
        System.out.println(OrderStatusEnums.CANCLE.getCode()+"     "+result.getOrderStatus());
        Assert.assertEquals(OrderStatusEnums.CANCLE.getCode(),result.getOrderStatus());
    }

    @Test
    public void finish() {
        OrderDTO orderDTO = orderServiceImpl.findOne("1528005110654919467");
        OrderDTO result = orderServiceImpl.finish(orderDTO);
        Assert.assertEquals(OrderStatusEnums.FINISHED.getCode(),result.getOrderStatus());
    }

    @Test
    public void find(){
        PageRequest pageRequest = new PageRequest(0,2);
        Page<OrderDTO> orderDTOPage = orderServiceImpl.findList(pageRequest);
        Assert.assertNotEquals(0,orderDTOPage.getTotalElements());
    }
}