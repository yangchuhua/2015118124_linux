package com.sell.demo.service.impl;

import com.sell.demo.enums.ProductStatusEnums;
import com.sell.demo.enums.ResultEnums;
import com.sell.demo.polo.ProductInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductInfoServiceImplTest {
    @Autowired
    ProductInfoServiceImpl productInfoServiceImpl;

    @Test
    public void findById() {
        ProductInfo productInfo = productInfoServiceImpl.findById("123");
        Assert.assertEquals("黑米粥",productInfo.getProductName());
    }

    @Test
    public void findAll() {
        PageRequest pageRequest = new PageRequest(0,2);

        Page<ProductInfo> productInfoList = productInfoServiceImpl.findAll(pageRequest);
        Assert.assertEquals(2,productInfoList.getTotalElements());

    }

    @Test
    public void findUpAll() {
        List<ProductInfo> productInfoList = productInfoServiceImpl.findUpAll();
        Assert.assertEquals(0,productInfoList.size());
    }

    @Test
    public void save() {
        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductId("125");
        productInfo.setProductName("虾粥");
        productInfo.setProductPrice(new BigDecimal(3.5));
        productInfo.setProductStock(2);
        productInfo.setProductDescription("好吃的小虾粥");
        productInfo.setProductIcon("http:xx");
        productInfo.setProductStatus(1);
        productInfoServiceImpl.save(productInfo);
    }

    @Test
    public void offSale(){
        ProductInfo productInfo = productInfoServiceImpl.OffSale("124");
        Assert.assertEquals(ProductStatusEnums.DOWN.getCode(),new Integer(productInfo.getProductStatus()));

    }

    @Test
    public void OnSale(){
        ProductInfo productInfo = productInfoServiceImpl.OnSale("123");
        Assert.assertEquals(ProductStatusEnums.up.getCode(),new Integer(productInfo.getProductStatus()));
    }
}