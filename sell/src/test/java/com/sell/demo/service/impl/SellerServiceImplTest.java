package com.sell.demo.service.impl;

import com.sell.demo.polo.SellerInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.parsers.SAXParser;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SellerServiceImplTest {
    @Autowired SellerServiceImpl sellerServiceImpl;

    @Test
    public void findByUserName(){
        SellerInfo sellerInfo = sellerServiceImpl.findSellerInfoByUsernameAndPassword("杨楚哗","杨楚哗");
        Assert.assertEquals("yangchuhua",sellerInfo.getOpenid());
    }
}