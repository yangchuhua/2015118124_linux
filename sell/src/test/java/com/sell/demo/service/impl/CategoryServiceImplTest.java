package com.sell.demo.service.impl;

import com.sell.demo.polo.ProductCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceImplTest {
    @Autowired
    CategoryServiceImpl categoryServiceImpl;
    @Test
    public void findById() {
        ProductCategory productCategory =categoryServiceImpl.findById(1);
        Assert.assertEquals(new Integer(1),productCategory.getCategoryId());
    }

    @Test
    public void findAll() {
        List<ProductCategory> list = categoryServiceImpl.findAll();
        Assert.assertEquals(new Integer(3),new Integer(list.size()));
    }

    @Test
    public void save() {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setCategoryName("坚果");
        productCategory.setCategoryType(6);
        categoryServiceImpl.save(productCategory);
    }

    @Test
    public void findByCategoryTypeIn() {
        List<Integer> list = Arrays.asList(4,5);
        List<ProductCategory> result = categoryServiceImpl.findByCategoryTypeIn(list);
        Assert.assertEquals(new Integer(2),new Integer(result.size()));
    }
}