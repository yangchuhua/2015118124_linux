
<html>

     <#include "../commom/header.ftl">
    <body>
    <div id="wrapper" class="toggled">

        <#include "../commom/nav.ftl">
            <div id="page-content-wrapper" >
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-11 column">
                        <table class="table table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th>类目Id</th>
                                <th>类目名称</th>
                                <th>类目编号</th>
                                <th>创建时间</th>
                                <th>更新时间</th>
                                <th>操作</th>

                            </tr>
                            </thead>
                            <tbody>
                                <#list productCategoryList as categoryList>
                                    <tr>
                                        <td>${categoryList.categoryId}</td>
                                        <td>${categoryList.categoryName}</td>
                                        <td>${categoryList.categoryType}</td>
                                        <td>${categoryList.createTime}</td>
                                        <td>${categoryList.updateTime}</td>
                                        <td><a href="/sell/seller/category/index?categoryId=${categoryList.categoryId}">修改</a></td>
                                    </tr>
                                </#list>
                            </tbody>
                        </table>
                    </div>
       
                </div>
            </div>

        </div>
    </div>
    </body>
</html>