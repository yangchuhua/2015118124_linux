
<html>

     <#include "../commom/header.ftl">
    <body>
    <div id="wrapper" class="toggled">

        <#include "../commom/nav.ftl">
            <div id="page-content-wrapper" >
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-11 column">
                        <table class="table table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th>商品ID</th>
                                <th>名称</th>
                                <th>图片</th>
                                <th>单价/th>
                                <th>库存</th>
                                <th>描述</th>
                                <th>类目</th>
                                <th>创建时间</th>
                                <th colspan="2">操作</th>

                            </tr>
                            </thead>
                            <tbody>
                     <#list productInfoPage.content as productInfo>
                     <tr>
                         <td>${productInfo.productId}</td>
                         <td>${productInfo.productName}</td>
                         <td><img src="${productInfo.productIcon}" height="50" width="50"></td>
                         <td>${productInfo.productPrice}</td>
                         <td>${productInfo.productStock}</td>
                         <td>${productInfo.productDescription}</td>
                         <td>${productInfo.categoryType}</td>
                         <td>${productInfo.createTime}</td>
                         <td>
                             <a href="/sell/seller/product/index?productId=${productInfo.productId}">修改</a>
                         </td>
                         <td>
                         <#--<#if orderDTO.getByOrderCode().getMessage() != "已取消" >-->

                         <#--</#if>-->
                             <#if productInfo.getProductStatusEnums().message == "在架">
                                  <a href="/sell/seller/product/offSale?productId=${productInfo.productId}">下架</a>
                             <#else >
                                    <a href="/sell/seller/product/onSale?productId=${productInfo.productId}">上架架</a>
                             </#if>
                         </td>

                     </tr>
                     </#list>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 column">
                        <ul class="pagination pull-right">
                    <#if currentPage lte 1>
                        <li class="disabled"><a href="#">上一页</a></li>
                    <#else >
                        <li ><a href="/sell/seller/product/list?page=${currentPage-1}&size=5">上一页</a></li>
                    </#if>
                    <#list 1..productInfoPage.getTotalPages() as index>
                        <#if currentPage == index>
                        <li class="disabled"><a href="#" >${index}</a></li>
                        <#else >
                        <li><a href="/sell/seller/product/list?page=${index}&size=5" >${index}</a></li>

                        </#if>
                    </#list>
                     <#if currentPage gte productInfoPage.getTotalPages()>
                        <li class="disabled"><a href="#">下一页</a></li>
                     <#else >
                        <li ><a href="/sell/seller/product/list?page=${currentPage+1}&size=5">下一页</a></li>
                     </#if>

                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </body>
</html>