package com.sell.demo.exception;

import com.sell.demo.enums.ResultEnums;

public class SellException extends RuntimeException {
    private Integer code;

    public SellException(ResultEnums resultEnums) {
        super(resultEnums.getMessage());
        this.code = code;
    }
}
