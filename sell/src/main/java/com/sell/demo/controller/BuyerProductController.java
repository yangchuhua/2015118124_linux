package com.sell.demo.controller;

import com.sell.demo.polo.ProductCategory;
import com.sell.demo.polo.ProductInfo;
import com.sell.demo.service.CategoryService;
import com.sell.demo.service.ProductInfoService;
import com.sell.demo.util.ResultVOUtil;
import com.sell.demo.vo.ProductInfoVO;
import com.sell.demo.vo.ProductVO;
import com.sell.demo.vo.ResultVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/buyer/product")
public class BuyerProductController {
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductInfoService productInfoService;
    @GetMapping("/list")
    public Map<String,Object> getResultVO(){
        Map<String ,Object> map = new HashMap<String ,Object>();
        //查询上架商品
        List<ProductInfo> productInfoList = productInfoService.findUpAll();
        //System.out.print(productInfoList);
        //查询类目
        List<Integer> categoryTypeList = new ArrayList<>();
        categoryTypeList = productInfoList.stream().map(e -> e.getCategoryType()).collect(Collectors.toList());
        List<ProductCategory> productCategoryList = categoryService.findByCategoryTypeIn(categoryTypeList);
        //拼接
        List<ProductVO> productVOList = new ArrayList<>();
        for (ProductCategory productCategory:productCategoryList){
            ProductVO productVO = new ProductVO();
            productVO.setCategoryName(productCategory.getCategoryName());
            productVO.setCategoryType(productCategory.getCategoryType());

            List<ProductInfoVO> productInfoVOList = new ArrayList<>();
            for (ProductInfo productInfo:productInfoList){
                if (productInfo.getCategoryType() == productCategory.getCategoryType()) {
                    ProductInfoVO productInfoVO = new ProductInfoVO();
                    BeanUtils.copyProperties(productInfo, productInfoVO);
                    productInfoVOList.add(productInfoVO);
                }
            }
            productVO.setFoods(productInfoVOList);
            productVOList.add(productVO);
        }
       // System.out.println("aasa"+"    "+productVOList);
        map.put("goods",productVOList);
        return map;
    }










    @GetMapping("/list1")
    public Map<String,Object> productList(){
        Map<String ,Object> map = new HashMap<String ,Object>();
        //查询上架商品
        List<ProductInfo> productInfoList = productInfoService.findUpAll();
       // System.out.println(productInfoList);
       map.put("list",productInfoList);
       return map;
    }
}
