package com.sell.demo.controller;

import com.sell.demo.dao.ProductCategoryDao;
import com.sell.demo.polo.ProductCategory;
import com.sell.demo.service.CategoryService;
import com.sell.demo.service.ProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
@RequestMapping("/seller/category")
public class SellerCategoryController {
    @Autowired
    private CategoryService categoryService;
    @GetMapping("/list")
    public ModelAndView list(Map<String,Object> map){
        List<ProductCategory> productCategoryList = categoryService.findAll();
        map.put("productCategoryList",productCategoryList);
        return new ModelAndView("category/list",map);
    }

    @GetMapping("/index")
    public ModelAndView index(@RequestParam(value = "categoryId",required = false)Integer categoryId,
                              Map<String,Object> map){
        ProductCategory productCategory = new ProductCategory();
        if (categoryId != null){
            productCategory = categoryService.findById(categoryId);
            map.put("productCategory",productCategory);
        }
        return new ModelAndView("category/index",map);
    }
    @PostMapping("/save")
    public ModelAndView save(@RequestParam("categoryId")Integer categoryId,
                             @RequestParam("categoryName")String categoryName,
                             @RequestParam("categoryType")Integer categoryType,
                             Map<String,Object> map){
        ProductCategory productCategory = new ProductCategory();
        if(categoryId != null){
            productCategory = categoryService.findById(categoryId);
        }
        productCategory.setCategoryName(categoryName);
        productCategory.setCategoryType(categoryType);
        ProductCategory result = categoryService.save(productCategory);
        map.put("message","修改类目成功");
        map.put("url","/sell/seller/category/list");
        return new ModelAndView("commom/success");
    }
}
