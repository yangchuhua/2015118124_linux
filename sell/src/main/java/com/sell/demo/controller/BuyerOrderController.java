package com.sell.demo.controller;

import com.sell.demo.converter.OrderForm2OrderDtoConverter;
import com.sell.demo.dto.OrderDTO;
import com.sell.demo.enums.ResultEnums;
import com.sell.demo.exception.SellException;
import com.sell.demo.form.OrderForm;
import com.sell.demo.service.BuyerService;
import com.sell.demo.service.OrderService;
import com.sell.demo.util.ResultVOUtil;
import com.sell.demo.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;

@RestController
@RequestMapping("/buyer/order")
public class BuyerOrderController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private BuyerService buyerService;

    @GetMapping("/find")
    public OrderDTO find(@RequestParam("orderid") String orderid){
        OrderDTO orderDTO = orderService.findOne(orderid);
        return orderDTO;
    }
    //创建订单
    @RequestMapping(value = "/create" ,method = RequestMethod.GET)
    public Map<String,Object> create(@Valid OrderForm orderForm, BindingResult bindingResult){
        System.out.println("下单了");
        Map<String ,Object> map = new HashMap<String ,Object>();
        if (bindingResult.hasErrors())
            throw new SellException(ResultEnums.PARAM_EEROR);
        OrderDTO orderDTO = OrderForm2OrderDtoConverter.convert(orderForm);
        if(CollectionUtils.isEmpty(orderDTO.getOrderDetailList()))
            throw new SellException(ResultEnums.CART_EMPTY);
        OrderDTO orderResult = orderService.create(orderDTO);

        map.put("success",orderResult.getOrderId());
        map.put("orderid",orderResult.getOrderId());
        return map;


    }
    //订单列表
    @GetMapping("/list")
    public Map<String,Object> list(@RequestParam("openid") String openid,
                                         @RequestParam(value = "page",defaultValue = "0")Integer page,
                                         @RequestParam(value = "size" ,defaultValue = "10") Integer size){
        Map<String ,Object> map = new HashMap<String ,Object>();
        if (openid == null )
            throw new SellException(ResultEnums.PARAM_EEROR);
        PageRequest pageRequest = new PageRequest(page,size);
        Page<OrderDTO> orderDTOPage = orderService.findList(openid,pageRequest);
        map.put("success",orderDTOPage.getContent());
        return map;
    }
    //订单详情
    @GetMapping("/detail")
    public Map<String,Object> detail(@RequestParam("openid") String openid,@RequestParam("orderid") String orderid){
        Map<String ,Object> map = new HashMap<String ,Object>();
        OrderDTO orderDTO = buyerService.findOrderOne(openid,orderid);
        map.put("success",orderDTO);
        return map;
    }
    //取消订单
    @PostMapping("/cancle")
    public ResultVO cancle(@RequestParam("openid") String openid,@RequestParam("orderid") String orderid){
        OrderDTO orderDTO = buyerService.cancle(openid,orderid);
        return ResultVOUtil.success();
    }
}
