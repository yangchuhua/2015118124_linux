package com.sell.demo.controller;



import com.sell.demo.enums.ProductStatusEnums;
import com.sell.demo.enums.ResultEnums;
import com.sell.demo.exception.SellException;
import com.sell.demo.form.ProductForm;
import com.sell.demo.polo.ProductCategory;
import com.sell.demo.polo.ProductInfo;
import com.sell.demo.service.CategoryService;
import com.sell.demo.service.ProductInfoService;
import com.sell.demo.util.KeyUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.rmi.MarshalledObject;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
@RequestMapping("/seller/product")
public class SellerProductController {
    @Autowired
    private ProductInfoService productInfoService;
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/list")
    public ModelAndView list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                             @RequestParam(value = "size",defaultValue = "5")Integer size,
                             Map<String,Object> map){
        PageRequest pageRequest = new PageRequest(page-1,size);
        Page<ProductInfo> productInfoPage = productInfoService.findAll(pageRequest);
        map.put("productInfoPage",productInfoPage);
        map.put("currentPage",page);
        map.put("size",size);
        return new ModelAndView("product/list",map);
    }

    @GetMapping("/onSale")
    public ModelAndView onSale(@RequestParam("productId") String productId,
                               Map<String,Object> map){
        ProductInfo productInfo = new ProductInfo();
        try {
            productInfo = productInfoService.OnSale(productId);
        }catch(Exception e){
            map.put("message", ResultEnums.PRODUCT_ONSALE_ERROE);
            map.put("url","/sell/seller/product/list");
            return new ModelAndView("commom/error",map);
        }
        map.put("message",ResultEnums.PRODUCT_ONSALE_SUCCESS.getMessage());
        map.put("url","/sell/seller/product/list");
        return new ModelAndView("commom/success");
    }



    @GetMapping("/offSale")
    public ModelAndView offSale(@RequestParam("productId") String productId,
                                Map<String,Object> map){
        ProductInfo productInfo = new ProductInfo();
        try {
            productInfo = productInfoService.OffSale(productId);
        }catch (Exception e){
            map.put("message",ResultEnums.PRODUCT_OFFSALE_ERROR);
            map.put("url","/sell/seller/product/list");
            return new ModelAndView("product/list",map);
        }
        map.put("message",ResultEnums.PRODUCT_OFFSALE_SUCCESS.getMessage());
        map.put("url","/sell/seller/product/list");
        return new ModelAndView("commom/success");
    }

    @GetMapping("/index")
    public ModelAndView index(@RequestParam(value = "productId",required = false)String productId,
                              Map<String,Object> map){
        if(! StringUtils.isEmpty(productId)){
            ProductInfo productInfo = productInfoService.findById(productId);
            map.put("productInfo",productInfo);
        }

        List<ProductCategory> productCategoryList = categoryService.findAll();
        map.put("productCategoryList",productCategoryList);
        return new ModelAndView("product/index",map);
    }

    @PostMapping("/save")
    public ModelAndView save(@Valid ProductForm productForm, BindingResult bindingResult,
                             Map<String,Object> map){
        if(bindingResult.hasErrors()){
            map.put("message",bindingResult.getFieldError().getDefaultMessage());
            map.put("url","/sell/seller/product/index");
            return new ModelAndView("commom/error",map);
        }
        ProductInfo productInfo = new ProductInfo();
        try{
            if (!StringUtils.isEmpty(productForm.getProductId())){
                productInfo = productInfoService.findById(productForm.getProductId());
            }else {
                productForm.setProductId(KeyUtil.genUniqueKey());
            }
            BeanUtils.copyProperties(productForm,productInfo);
            productInfoService.save(productInfo);
        }catch (Exception e){
            map.put("message",ResultEnums.UPDATE_PRODUCT_ERROR.getMessage());
            map.put("url","/sell/seller/product/index");
            return new ModelAndView("commom/error",map);
        }
        map.put("message",ResultEnums.UPDATE_PRODUCT_SUCCESS.getMessage());
        map.put("url","/sell/seller/product/list");
        return new ModelAndView("commom/success");
    }


}
