package com.sell.demo.controller;

import com.sell.demo.dto.OrderDTO;
import com.sell.demo.enums.ResultEnums;
import com.sell.demo.exception.SellException;
import com.sell.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@RequestMapping("/seller/order")
public class SellerOrderController {
    @Autowired
    private OrderService orderService;



    @GetMapping("/list")
    public ModelAndView list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                             @RequestParam(value = "size",defaultValue = "10")Integer size,
                             Map<String,Object> map){
        PageRequest pageRequest = new PageRequest(page-1,size);
        Page<OrderDTO> orderDTOPage = orderService.findList(pageRequest);
        map.put("orderDTOPage",orderDTOPage);
        //orderDTOPage.getTotalPages()
        //orderDTOPage.getTotalPages()
        map.put("currentPage",page);
        return new ModelAndView("order/list",map);
    }

    @GetMapping("/cancle")
    public ModelAndView cancle(@RequestParam("orderid") String orderid,Map<String,Object> map){
        try {
            OrderDTO orderDTO = orderService.findOne(orderid);
           // orderDTO.getByOrderCode().getMessage();
            orderService.cancle(orderDTO);
        }catch (Exception e){
            map.put("message",ResultEnums.ORDERMASTER_NOT_EXIT.getMessage());
            map.put("url","/sell/seller/order/list");
            return new ModelAndView("commom/error",map);
        }
        map.put("message", ResultEnums.SUCCESS.getMessage());
        map.put("url","/sell/seller/order/list");
        return new ModelAndView("commom/success");
    }


    @GetMapping("/detail")
    public ModelAndView detail(@RequestParam("orderid") String orderid,
                               Map<String,Object> map){
        OrderDTO orderDTO = new OrderDTO();
        try{
             orderDTO = orderService.findOne(orderid);
        }catch (Exception e){
            map.put("message",ResultEnums.ORDERMASTER_NOT_EXIT.getMessage());
            map.put("url","/sell/seller/order/list");
            return new ModelAndView("commom/error",map);
        }
        map.put("orderDTO",orderDTO);
        return new ModelAndView("order/detail");
    }

    @GetMapping("finish")
        public ModelAndView finish(@RequestParam("orderid")String orderid,
                                   Map<String,Object> map){
        OrderDTO orderDTO = new OrderDTO();
        try{
            orderDTO = orderService.findOne(orderid);
            orderService.finish(orderDTO);
        }catch (Exception e){
            map.put("message",ResultEnums.ORDERMASTER_NOT_EXIT.getMessage());
            map.put("url","/sell/seller/order/list");
            return new ModelAndView("commom/error",map);
        }
        map.put("message", ResultEnums.FINISH_ORDER_SUCCESS.getMessage());
        map.put("url","/sell/seller/order/list");
        return new ModelAndView("commom/success");
        }
}
