package com.sell.demo.converter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sell.demo.dto.OrderDTO;
import com.sell.demo.enums.ResultEnums;
import com.sell.demo.exception.SellException;
import com.sell.demo.form.OrderForm;
import com.sell.demo.polo.OrderDetail;

import java.util.ArrayList;
import java.util.List;

public class OrderForm2OrderDtoConverter {
    public static OrderDTO convert(OrderForm orderForm){
        Gson gson = new Gson();
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setBuyerName(orderForm.getName());
        orderDTO.setBuyerPhone(orderForm.getPhone());
        orderDTO.setBuyerAddress(orderForm.getAddress());
        orderDTO.setBuyerOpenid(orderForm.getOpenid());
        //System.out.println("下单资料  "+ orderForm.getName()+"  "+orderForm.getPhone());
        List<OrderDetail> orderDetailList = new ArrayList<>();
        try {
           orderDetailList = gson.fromJson(orderForm.getItems(), new TypeToken<List<OrderDetail>>() {
            }.getType());
        }catch (Exception e){
            throw new SellException(ResultEnums.PARAM_EEROR);
        }
        orderDTO.setOrderDetailList(orderDetailList);
        return orderDTO;


    }
}
