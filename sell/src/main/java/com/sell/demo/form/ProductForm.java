package com.sell.demo.form;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

//存储表单提交过来的字段
@Data
public class ProductForm {
    private String productId;
    private String productName;
    private BigDecimal productPrice;
    private int productStock;
    private String productDescription;
    private String productIcon;
    private int categoryType;

}
