package com.sell.demo.form;

import com.sell.demo.polo.OrderDetail;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;
@Data
public class OrderForm {
    @NotNull(message = "名字不能为空")
    private String name;

    @NotNull(message = "电话不能为空")
    private String phone;

    @NotNull(message = "地址不能为空")
    private String address;

    @NotNull(message = "openid不能为空")
    private String openid;

    @NotNull(message = "购物车不能为空")
    private String items;
}
