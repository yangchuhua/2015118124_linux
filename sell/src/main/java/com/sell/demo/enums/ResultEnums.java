package com.sell.demo.enums;

import lombok.Getter;

@Getter
public enum ResultEnums {
    SUCCESS(0,"成功"),
    PARAM_EEROR(1,"参数不正确"),
    CART_EMPTY(2,"购物车为空"),
    ORDER_OWNER_ERROR(3,"用户openid错误"),
    PRODUCT_NOT_EXIT(10,"商品不存在"),
    PRODUCT_STOCK_ERROR(11,"商品库存错误"),
    ORDERMASTER_NOT_EXIT(12,"订单不存在"),
    ORDERDETAIL_NOT_EXIT(13,"订单详情表不存在"),
    ORDER_NOT_EXIT(18,"订单不存在"),
    ORDER_STATUS_ERROR(14,"订单状态不正确"),
    ORDER_UPDATE_FAIL(15,"修改订单状态失败"),
    ORDER_DETAIL_EMPTY(16,"订单中没有商品详情"),
    ORDER_PAY_STATUS_ERROR(17,"订单支付状态不正确"),
    FINISH_ORDER_SUCCESS(18,"完结订单成功"),
    PRODUCT_STATUS_ERROR(19,"商品状态不正确"),
    PRODUCT_ONSALE_ERROE(20,"上架商品失败"),
    PRODUCT_OFFSALE_ERROR(21,"下架商品失败"),
    PRODUCT_OFFSALE_SUCCESS(22,"上架商品成功"),
    PRODUCT_ONSALE_SUCCESS(23,"下架商品成功"),
    UPDATE_PRODUCT_SUCCESS(24,"修改商品成功"),
    UPDATE_PRODUCT_ERROR(25,"修改商品失败"),
    ;
    private Integer code;
    private String message;

    ResultEnums(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
