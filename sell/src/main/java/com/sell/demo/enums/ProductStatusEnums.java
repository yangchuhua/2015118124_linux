package com.sell.demo.enums;

import lombok.Data;
import lombok.Getter;
import org.aspectj.apache.bcel.classfile.Code;

@Getter
public enum ProductStatusEnums implements CodeEnums {

    up(1,"在架"),
    DOWN(0,"下架")
    ;
    private Integer code;
    private String message;

    ProductStatusEnums(Integer code,String message){
        this.code = code;
        this.message = message;
    }

}
