package com.sell.demo.service.impl;

import com.sell.demo.dao.ProductInfoDao;
import com.sell.demo.dto.CartDTO;
import com.sell.demo.enums.ProductStatusEnums;
import com.sell.demo.enums.ResultEnums;
import com.sell.demo.exception.SellException;
import com.sell.demo.polo.ProductInfo;
import com.sell.demo.service.ProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProductInfoServiceImpl implements ProductInfoService {
    @Autowired
    private ProductInfoDao productInfoDao;

    @Override
    public ProductInfo findById(String productId) {
        return productInfoDao.findById(productId).get();
    }

    @Override
    public Page<ProductInfo> findAll(Pageable pageable) {
        return productInfoDao.findAll(pageable);
    }

    @Override
    public List<ProductInfo> findUpAll() {
        return productInfoDao.findByProductStatus(ProductStatusEnums.up.getCode());
    }

    @Override
    public ProductInfo save(ProductInfo productInfo) {
        return productInfoDao.save(productInfo);
    }

    @Override
    @Transactional
    public void increaseStock(List<CartDTO> cartDTOList) {
        for (CartDTO cartDTO:cartDTOList){
            ProductInfo productInfo = productInfoDao.findById(cartDTO.getProductId()).get();
            if(productInfo == null)
                throw new SellException(ResultEnums.PRODUCT_NOT_EXIT);
            int result = productInfo.getProductStock()+cartDTO.getProductQuantity();
            productInfo.setProductStock(result);
            productInfoDao.save(productInfo);
        }
    }

    @Override
    @Transactional
    public void descreaseStock(List<CartDTO> cartDTOList) {

        for(CartDTO cartDTO:cartDTOList){
            ProductInfo productInfo = productInfoDao.findById(cartDTO.getProductId()).get();
            int result = productInfo.getProductStock() - cartDTO.getProductQuantity();
            if(result <0){
                throw  new SellException(ResultEnums.PRODUCT_STOCK_ERROR);
            }
            productInfo.setProductStock(result);
            System.out.println("result  "+result);
            productInfoDao.save(productInfo);
        }

    }

    @Override
    public ProductInfo OffSale(String productId) {
        ProductInfo productInfo = productInfoDao.findById(productId).get();
        if(productInfo == null)
            throw new SellException(ResultEnums.PRODUCT_NOT_EXIT);
        if(productInfo.getProductStatus() == ProductStatusEnums.DOWN.getCode())
            throw new SellException(ResultEnums.PRODUCT_STATUS_ERROR);
        productInfo.setProductStatus(ProductStatusEnums.DOWN.getCode());
        productInfoDao.save(productInfo);

        return productInfo;
    }

    @Override
    public ProductInfo OnSale(String productId) {
        ProductInfo productInfo = productInfoDao.findById(productId).get();
        if (productInfo == null)
            throw new SellException(ResultEnums.PRODUCT_NOT_EXIT);
        if(productInfo.getProductStatus() == ProductStatusEnums.up.getCode())
            throw new SellException(ResultEnums.PRODUCT_STATUS_ERROR);
        productInfo.setProductStatus(ProductStatusEnums.up.getCode());
        productInfoDao.save(productInfo);
        return productInfo;
    }
}
