package com.sell.demo.service;

import com.sell.demo.dto.CartDTO;
import com.sell.demo.polo.ProductInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductInfoService {
    ProductInfo findById(String productId);
    Page<ProductInfo> findAll(Pageable pageable);
    List<ProductInfo> findUpAll();
    ProductInfo save(ProductInfo productInfo);
    //加库存减库存
    void increaseStock(List<CartDTO> cartDTOList);
    void descreaseStock(List<CartDTO> cartDTOList);

    //商品上下架
    ProductInfo OffSale(String productId);
    ProductInfo OnSale(String productId);

}
