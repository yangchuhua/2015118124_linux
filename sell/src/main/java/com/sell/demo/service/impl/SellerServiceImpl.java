package com.sell.demo.service.impl;

import com.sell.demo.dao.SellInfoDao;
import com.sell.demo.polo.SellerInfo;
import com.sell.demo.service.SellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SellerServiceImpl implements SellerService {
    @Autowired
    private SellInfoDao sellInfoDao;

    @Override
    public SellerInfo findSellerInfoByUsernameAndPassword(String username, String password) {
        return sellInfoDao.findByUsernameAndPassword(username,password);
    }
}
