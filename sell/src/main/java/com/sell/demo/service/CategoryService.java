package com.sell.demo.service;

import com.sell.demo.polo.ProductCategory;

import java.util.List;

public interface CategoryService {
    //后台
    ProductCategory findById(Integer categoryId);
    List<ProductCategory> findAll();
    ProductCategory save(ProductCategory productCategory);
    //买家
    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);

}
