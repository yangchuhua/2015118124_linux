package com.sell.demo.service;

import com.sell.demo.dto.OrderDTO;

public interface BuyerService {
    OrderDTO findOrderOne(String openid,String orderid);
    OrderDTO cancle(String openid,String orderid);
}
