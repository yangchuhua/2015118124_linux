package com.sell.demo.service.impl;

import com.sell.demo.dto.OrderDTO;
import com.sell.demo.enums.ResultEnums;
import com.sell.demo.exception.SellException;
import com.sell.demo.service.BuyerService;
import com.sell.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BuyerServiceImpl implements BuyerService{
    @Autowired
    private OrderService orderService;
    @Override
    public OrderDTO findOrderOne(String openid, String orderid) {
        return checkOwner(openid,orderid);
    }

    @Override
    public OrderDTO cancle(String openid, String orderid) {
        OrderDTO orderDTO = checkOwner(openid,orderid);
        if(orderDTO == null )
            throw new SellException(ResultEnums.ORDER_NOT_EXIT);

        return orderService.cancle(orderDTO);
    }

    private OrderDTO checkOwner(String openid,String orderid){
        OrderDTO orderDTO = orderService.findOne(orderid);
        if(orderDTO == null)
            return null;
        if(! orderDTO.getBuyerOpenid().equalsIgnoreCase(openid))
            throw new SellException(ResultEnums.ORDER_OWNER_ERROR);
        return orderDTO;
    }
}
