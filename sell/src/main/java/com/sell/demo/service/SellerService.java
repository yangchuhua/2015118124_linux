package com.sell.demo.service;

import com.sell.demo.polo.SellerInfo;

public interface SellerService {
    SellerInfo findSellerInfoByUsernameAndPassword(String usernama, String password);
}
