package com.sell.demo.service.impl;

import com.sell.demo.converter.OrderMaster2OrderDtoConverter;
import com.sell.demo.dao.OrderDetailDao;
import com.sell.demo.dao.OrderMasterDao;
import com.sell.demo.dto.CartDTO;
import com.sell.demo.dto.OrderDTO;
import com.sell.demo.enums.OrderStatusEnums;
import com.sell.demo.enums.PayStatusEnums;
import com.sell.demo.enums.ProductStatusEnums;
import com.sell.demo.enums.ResultEnums;
import com.sell.demo.exception.SellException;
import com.sell.demo.polo.OrderDetail;
import com.sell.demo.polo.OrderMaster;
import com.sell.demo.polo.ProductInfo;
import com.sell.demo.service.OrderService;
import com.sell.demo.service.ProductInfoService;
import com.sell.demo.service.WebSocket;
import com.sell.demo.util.KeyUtil;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private ProductInfoService productInfoService;
    @Autowired
    private OrderDetailDao orderDetailDao;
    @Autowired
    private OrderMasterDao orderMasterDao;
    @Autowired
    private WebSocket webSocket;
    @Override
    public OrderDTO create(OrderDTO orderDTO) {
        /* 创建订单
        * 1.创建订单，首先要查询商品是否存在
        * 2.如果存在，计算总价
        * 3.写入数据库
        * 4.口库存
        * */
        String orderDetailId = KeyUtil.genUniqueKey();
        BigDecimal productAmount = new BigDecimal(0);
        //查询商品
        for (OrderDetail orderDetail: orderDTO.getOrderDetailList()){
            ProductInfo productInfo = productInfoService.findById(orderDetail.getProductId());
            if(productInfo == null){
                throw new SellException(ResultEnums.PRODUCT_NOT_EXIT);
            }
            //计算总价
            productAmount = productInfo.getProductPrice().multiply(new BigDecimal(orderDetail.getProductQuantity())).add(productAmount);
            //写进orderDetail数据库
            orderDetail.setDetailId(KeyUtil.genUniqueKey());
            orderDetail.setOrderId(orderDetailId);
            BeanUtils.copyProperties(productInfo,orderDetail);
            orderDetailDao.save(orderDetail);
        }
        //写入orderMaster数据库
        OrderMaster orderMaster = new OrderMaster();

        orderDTO.setPayStatus(PayStatusEnums.WAIT.getCode());
        orderDTO.setOrderStatus(OrderStatusEnums.NEW.getCode());
        orderDTO.setOrderId(orderDetailId);
        orderDTO.setOrderAmount(productAmount);
        BeanUtils.copyProperties(orderDTO,orderMaster);

        orderMasterDao.save(orderMaster);
        //减库存
        List<CartDTO> cartDTOList = new ArrayList<>();
        cartDTOList = orderDTO.getOrderDetailList().stream().map(e -> new CartDTO(e.getProductId(),e.getProductQuantity())).collect(Collectors.toList());
        productInfoService.descreaseStock(cartDTOList);
        //推送消息
        webSocket.senfMessage(orderDTO.getOrderId());
        return orderDTO;
    }

    @Override
    public OrderDTO findOne(String orderId) {
        OrderMaster orderMaster = orderMasterDao.findById(orderId).get();
        if(orderMaster == null)
            throw new SellException(ResultEnums.ORDERMASTER_NOT_EXIT);
        OrderDTO orderDTO = new OrderDTO();
        List<OrderDetail> orderDetailList = new ArrayList<>();
        orderDetailList = orderDetailDao.findByOrderId(orderId);
        if (orderDetailList.size()<0)
            throw new SellException(ResultEnums.ORDERDETAIL_NOT_EXIT);

        BeanUtils.copyProperties(orderMaster,orderDTO);
        orderDTO.setOrderDetailList(orderDetailList);

        return orderDTO;
    }

    @Override
    public Page<OrderDTO> findList(String buyerOpenid, Pageable pageable) {
        Page<OrderMaster> orderMasterPage = orderMasterDao.findByBuyerOpenid(buyerOpenid,pageable);
        List<OrderDTO> orderDTOList = OrderMaster2OrderDtoConverter.convert(orderMasterPage.getContent());
        Page<OrderDTO> orderDTOPage = new PageImpl<>(orderDTOList,pageable,orderMasterPage.getTotalElements());

        return orderDTOPage;
    }

    @Override
    @Transactional
    public OrderDTO pay(OrderDTO orderDTO) {
        //判断订单状态
        if(! orderDTO.getOrderStatus().equals(OrderStatusEnums.NEW.getCode()))
            throw new SellException(ResultEnums.ORDER_STATUS_ERROR);
        //判断支付状态
        if(! orderDTO.getPayStatus().equals(PayStatusEnums.WAIT.getCode()))
            throw new SellException(ResultEnums.ORDER_PAY_STATUS_ERROR);
        //修改支付状态
        orderDTO.setPayStatus(PayStatusEnums.SUCCESS.getCode());
        OrderMaster orderMaster = new OrderMaster();
        BeanUtils.copyProperties(orderDTO,orderMaster);
        OrderMaster orderUpdate = orderMasterDao.save(orderMaster);
        if(orderUpdate == null)
            throw new SellException(ResultEnums.ORDER_UPDATE_FAIL);

        return orderDTO;
    }

    @Override
    @Transactional
    public OrderDTO cancle(OrderDTO orderDTO) {
        //判断订单状态，为新订单就可以取消
        OrderMaster orderMaster = new OrderMaster();

        if( orderDTO.getOrderStatus().equals(OrderStatusEnums.CANCLE.getCode() ))
            throw new SellException(ResultEnums.ORDER_STATUS_ERROR);
        //改变订单状态
        orderDTO.setOrderStatus(OrderStatusEnums.CANCLE.getCode());
        BeanUtils.copyProperties(orderDTO,orderMaster);

        OrderMaster orderResult = orderMasterDao.save(orderMaster);
        orderDTO.setOrderStatus(orderResult.getOrderStatus());
        orderDTO.setOrderDetailList(orderDetailDao.findByOrderId(orderDTO.getOrderId()));
        System.out.println(orderResult.getOrderStatus());
        if(orderResult == null)
            throw new SellException(ResultEnums.ORDER_UPDATE_FAIL);

        //加库存
        if(orderDTO.getOrderDetailList().size() ==0)
            throw new SellException(ResultEnums.ORDER_DETAIL_EMPTY);
        List<CartDTO> cartDTOList = orderDTO.getOrderDetailList().stream().
                map(e -> new CartDTO(e.getProductId(),e.getProductQuantity())).collect(Collectors.toList());
        productInfoService.increaseStock(cartDTOList);
        //退款
        //TODO
        System.out.println("aaa   "+"  "+orderDTO.getOrderStatus());
        return orderDTO;
    }

    @Override
    @Transactional
    public OrderDTO finish(OrderDTO orderDTO) {
        //查询订单状态
        if(! orderDTO.getOrderStatus().equals(OrderStatusEnums.NEW.getCode()))
            throw new SellException(ResultEnums.ORDER_STATUS_ERROR);

        //修改订单状态
        orderDTO.setOrderStatus(OrderStatusEnums.FINISHED.getCode());
        OrderMaster orderMaster = new OrderMaster();
        BeanUtils.copyProperties(orderDTO,orderMaster);
        OrderMaster orderUpdate = orderMasterDao.save(orderMaster);
        if (orderUpdate == null)
            throw new SellException(ResultEnums.ORDER_UPDATE_FAIL);
        return orderDTO;
    }

    @Override
    public Page<OrderDTO> findList(Pageable pageable) {
        Page<OrderMaster> orderMasterPage = orderMasterDao.findAll(pageable);
        List<OrderDTO> orderDTOList = OrderMaster2OrderDtoConverter.convert(orderMasterPage.getContent());

        return new PageImpl<OrderDTO>(orderDTOList,pageable,orderMasterPage.getTotalElements());
    }
}
