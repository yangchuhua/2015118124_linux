package com.sell.demo.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sell.demo.enums.OrderStatusEnums;
import com.sell.demo.enums.PayStatusEnums;
import com.sell.demo.polo.OrderDetail;
import com.sell.demo.util.CodeUtil;
import com.sell.demo.util.serialize.Date2LongSerialize;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDTO {
    private String orderId;
    private String buyerName;
    private String buyerPhone;
    private String buyerAddress;
    private String buyerOpenid;
    private BigDecimal orderAmount;
    private Integer orderStatus ;
    private Integer payStatus ;
    @JsonSerialize(using=Date2LongSerialize.class)
    private Date createTime;
    @JsonSerialize(using=Date2LongSerialize.class)
    private Date updateTime;

    List<OrderDetail> orderDetailList;

    @JsonIgnore
    public OrderStatusEnums getByOrderCode(){
        return CodeUtil.getByCode(orderStatus,OrderStatusEnums.class);
    }
    @JsonIgnore
    public PayStatusEnums getByPayStatus(){
        return CodeUtil.getByCode(payStatus,PayStatusEnums.class);
    }
}
