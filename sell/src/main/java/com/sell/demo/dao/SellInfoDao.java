package com.sell.demo.dao;

import com.sell.demo.polo.SellerInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SellInfoDao extends JpaRepository<SellerInfo,String> {
    SellerInfo findByUsernameAndPassword(String username,String password);
}
