package com.sell.demo.polo;

import com.sell.demo.enums.OrderStatusEnums;
import com.sell.demo.enums.PayStatusEnums;
import lombok.Data;
import lombok.Getter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@DynamicUpdate
@Data
public class OrderMaster {
    @Id
    private String orderId;
    private String buyerName;
    private String buyerPhone;
    private String buyerAddress;
    private String buyerOpenid;
    private BigDecimal orderAmount;
    private Integer orderStatus = OrderStatusEnums.NEW.getCode();
    private Integer payStatus = PayStatusEnums.WAIT.getCode();
    private Date createTime;
    private Date updateTime;

}
