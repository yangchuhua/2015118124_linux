package com.sell.demo.polo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sell.demo.enums.ProductStatusEnums;
import com.sell.demo.util.CodeUtil;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@DynamicUpdate
public class ProductInfo {
    @Id
    private String productId;
    private String productName;
    private BigDecimal productPrice;
    private int productStock;
    private String productDescription;
    private String productIcon;
    private int productStatus = ProductStatusEnums.up.getCode();
    private int categoryType;
    private int Count = 0;
    private Date createTime;
    private Date updateTime;
    @JsonIgnore
    public ProductStatusEnums getProductStatusEnums(){
        return CodeUtil.getByCode(productStatus,ProductStatusEnums.class);
    }
}
