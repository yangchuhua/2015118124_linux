package com.sell.demo.polo;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@DynamicUpdate
@Data
public class SellInfo {
    @Id
    private String id;
    private String username;
    private String password;
    private String openid;

}
