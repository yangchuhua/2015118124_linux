package com.sell.demo.util;

import com.sell.demo.enums.CodeEnums;

public class CodeUtil {
    public static <T extends CodeEnums> T getByCode(Integer code,Class<T> enumClass){
            for (T each: enumClass.getEnumConstants()){
                if(code.equals(each.getCode()))
                    return each;
            }
            return null;
    }
}
