package com.imooc.demo.dao;

import com.imooc.demo.entity.User;

import java.util.List;

public interface UserDao {
    List<User> query();
    int insert(User user);
    int update(User user);
    int delete(User user);

}
